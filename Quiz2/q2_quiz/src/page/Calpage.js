
import EvenOddResult from "../component/EvenOddResult";
import { useState } from "react";

import Button from '@mui/material/Button';

function Calpage(){

    const [ calresult, setcalReSult ] = useState("");
    const [ translateresult, settranslateResult ] = useState("");

    const [ numBer, setNumber ] = useState("");

    function CalculateNUM(){
        let n = parseInt(numBer);
        let cal = ( n );
        setcalReSult( cal );
        if( cal % 2 == 0 ) {
            settranslateResult("เป็นเลขคู่");
        }else {
            settranslateResult("เป็นเลขคี่");
        }
    }

    
    return(
        <div align="left">
            <div align="center">
                หาเลข คู่-คี่
                <hr />

                เลข: <input type="number"
                            value={numBer} 
                            onChange={ (e) => { setNumber(e.target.value); }}/> <br />

                <Button variant="contained" onclick={ ()=>{ CalculateNUM() } }> Calculate </Button>
                { calresult != 0 && 
                    <div>
                    <hr />
                    ผลการคำนวณ
                    <EvenOddResult
                        number = { numBer } 
                        cal = { calresult }
                        result = { translateresult }
                    />
                    </div>
                }
            </div>
        </div>
    );

}
export default Calpage;
