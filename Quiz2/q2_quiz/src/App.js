import logo from './logo.svg';
import './App.css';

import AboutUspage from './page/AboutUspage'
import Calpage from './page/Calpage'
import Header from './component/Header'

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
      <Route path="about" element={  
      <AboutUspage />
      } />

      <Route path="/" element={
        <Calpage />
      } />
      </Routes>
    </div>
  );
}

export default App;
